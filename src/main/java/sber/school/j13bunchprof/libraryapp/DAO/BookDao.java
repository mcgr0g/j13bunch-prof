package sber.school.j13bunchprof.libraryapp.DAO;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import sber.school.j13bunchprof.libraryapp.model.CustomerBook;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
@Scope("prototype")
public class BookDao {
    private final Connection connection;

    public BookDao(Connection connection) {
        this.connection = connection;
    }

    public List<CustomerBook> findBooksByCustomerId(Integer customerId) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement(
          "select * from customer_book where customer_id = ?"
        );
        selectQuery.setInt(1, customerId);
        ResultSet resultSet = selectQuery.executeQuery();
        List<CustomerBook> customerBookList = new ArrayList<>();
        while (resultSet.next()){
            CustomerBook book = new CustomerBook();
            book.setRecordId(resultSet.getInt("id"));
            book.setCustomerId(resultSet.getInt("customer_id"));
            book.setBookName(resultSet.getString("name"));
            customerBookList.add(book);
        }
        return customerBookList;
    }

}
