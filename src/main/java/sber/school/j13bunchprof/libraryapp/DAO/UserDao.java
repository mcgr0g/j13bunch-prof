package sber.school.j13bunchprof.libraryapp.DAO;

import org.springframework.stereotype.Component;
import sber.school.j13bunchprof.libraryapp.model.Customer;
import sber.school.j13bunchprof.libraryapp.model.CustomerBook;

import java.sql.*;
import java.util.List;

@Component
public class UserDao {
    private final Connection connection;

    private final BookDao bookDao;

    public UserDao(Connection connection, BookDao bookDao) {
        this.connection = connection;
        this.bookDao = bookDao;
    }

    public Customer findUserById(Integer customerId) throws SQLException{
        PreparedStatement selectQuery = connection
                .prepareStatement("select * from customer where id = ?");
        selectQuery.setInt(1, customerId);
        ResultSet resultSet = selectQuery.executeQuery();
        while (resultSet.next()){
            Customer customer = new Customer();
            customer.setCustomerId(resultSet.getInt("id"));
            customer.setFirstName(resultSet.getString("firstname"));
            customer.setLastName(resultSet.getString("lastname"));
            customer.setEmail(resultSet.getString("email"));
            customer.setBirthdate(resultSet.getDate("birthdate").toLocalDate());
            return customer;
        }
        return null;
    }

    public void addUser(Customer customer) throws SQLException {
        PreparedStatement insertQuery = connection
                .prepareStatement("insert into customer ("
                        + " firstname,"
                        + " lastname,"
                        + " birthdate,"
                        + " email) values "
                        +" ( ?, ?, ?, ?);");

        insertQuery.setString(1, customer.getFirstName());
        insertQuery.setString(2, customer.getLastName());
        insertQuery.setObject(3, customer.getBirthdate(), Types.DATE);
        insertQuery.setString(4, customer.getEmail());
        insertQuery.execute();
    }

    public List<CustomerBook> getUserBooks(String email) throws SQLException {
        PreparedStatement selectQuery = connection
                .prepareStatement("select * from customer where email = ?");
        selectQuery.setString(1, email);
        ResultSet resultSet = selectQuery.executeQuery();

        resultSet.next();

        return bookDao.findBooksByCustomerId(resultSet.getInt("id"));
    }

}
