package sber.school.j13bunchprof.libraryapp.constants;

public interface DBConstants {
    String DB_HOST = "localhost";

    String DB = "local_db";

    String USER = "postgres";

    String PASSWORD = "12345";

    String PORT = "5433";
}
