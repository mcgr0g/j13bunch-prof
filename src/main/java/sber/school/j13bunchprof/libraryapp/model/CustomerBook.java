package sber.school.j13bunchprof.libraryapp.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
public class CustomerBook {

//    @Setter(AccessLevel.NONE)
    private Integer recordId;

    private Integer customerId;

    private String bookName;

}
