package sber.school.j13bunchprof.libraryapp.model;

import lombok.*;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Customer {

//    @Setter(AccessLevel.NONE)
    private Integer customerId;

    private String firstName;

    private String lastName;

    private LocalDate birthdate;

    private String email;

}
