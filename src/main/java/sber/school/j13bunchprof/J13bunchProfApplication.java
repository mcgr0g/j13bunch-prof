package sber.school.j13bunchprof;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import sber.school.j13bunchprof.libraryapp.DAO.BookDao;
import sber.school.j13bunchprof.libraryapp.DAO.UserDao;
import sber.school.j13bunchprof.libraryapp.DBConfigContext;
import sber.school.j13bunchprof.libraryapp.model.Customer;
import sber.school.j13bunchprof.libraryapp.model.CustomerBook;

import java.time.LocalDate;
import java.util.List;

@SpringBootApplication
public class J13bunchProfApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(J13bunchProfApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(DBConfigContext.class);
        UserDao userDao = ctx.getBean(UserDao.class);
        BookDao bookDao = ctx.getBean((BookDao.class));
        System.out.println(userDao.findUserById(1));
        userDao.addUser(new Customer(4,
                "Иванов",
                "Иван",
                LocalDate.of(2002,2,3),
                "test@test.dev"));
        // в db.changelog есть fill скрипты
        System.out.println(userDao.findUserById(3));

        List<CustomerBook> customerBookList = userDao.getUserBooks("123@bk.ru");
        System.out.println(customerBookList);
    }

}
